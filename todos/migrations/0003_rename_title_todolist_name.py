# Generated by Django 4.2.1 on 2023-05-31 17:37

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todolist_delete_todo"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todolist",
            old_name="title",
            new_name="name",
        ),
    ]
